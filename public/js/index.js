$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#update').on('click', function () {
        location.reload();
    });

    let i = 0;

    $('#kill_sheep').on('click', function () {
        killSheep();
    });
    setInterval(function () {
        $.ajax({
            type: 'POST',
            url: '{{route("ajax_create_sheep")}}',
            success: function (response) {
                if (response.error ) {
                    let message = $('<h4 class="message_title" style="color:red">Ошибка сервера</h4>');
                    insetMessage(message);
                } else if(response.code === 0){
                    let message = $('<h4 class="message_title" style="color: #2a9055;" > В' + response.name_sheep_pen + ": родилась овца с именем " + response.name_sheep + '</h4>');
                    insetMessage(message);
                }else if(response.code === 1){
                    let message = $('<h4 class="message_title" style="color:#2a9055"> В' +
                        ''+ response.name_sheep_pen + ": родилась овца с именем " + response.name_sheep + '' +
                        ' После овца по имени '+ response.name_sheep_move +' была пермещена в '+response.name_from_sheep_pen +'</h4>');
                    insetMessage(message);
                }

            }
        });
    }, 10000);

    setInterval(function () {
        $.ajax({
            type: 'POST',
            url: '{{route("ajax_kill_sheep")}}',
            success: function (response) {
                console.log(response);
                if(!response.error) {
                    if (parseInt(response.code) === 0) {
                        console.log(response.code);
                        $('.message_title').remove();
                        $('.message').append('<h4 class="message_title" style="color:red">Хватит убивать в каждом загоне осталось по 1 овцы дождитесь рождения новой</h4>');
                        setTimeout(function () {
                            $('.message_title').remove()
                        }, 10000);
                    }else if (parseInt(response.code) === 1) {
                        let message =$('<h4 class="message_title" style="color:blue">' +
                            'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' </h4>');
                        insetMessage(message);
                    } else if (parseInt(response.code) === 2) {
                        let message =$('<h5 class="message_title" style="color:deeppink">' +
                            'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + '' +
                            'и в этот загон была подселена овца :' + response.name_sheep_move +' из '+response.name_from_sheep_pen +' </h5>');
                        insetMessage(message);
                    }else if (parseInt(response.code) === 3) {
                        let  message = $('<h5 class="message_title" style="color:pink">' +
                            'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' Пермещение не удалось осуществить из-за сбоев сервера </h5>');
                        insetMessage(message);
                    }
                }else{
                    $('.message').append('<h4 class="message_title" style="color:red">Ошибка сервера</h4>');
                    setTimeout(function () {
                        $('.message_title').remove()
                    }, 2000);
                }
            }
        });
    }, 100000);

    var getData = function () {
        $.ajax({
            type: 'POST',
            url: '{{route("ajax_index")}}',
            success: function (response) {

                $.each(response.data, function (key, value) {
                    $('#title_sheep_pen-' + value.sheep_pen.id).text(value.sheep_pen.name);
                    var sheep_pen = $('#sheep_pen_' + value.sheep_pen.id);

                    $.each(value.sheep, function (key1, sheep) {
                        sheep_pen.append('<p class="sheep_name" id="sheep-' + sheep.id + '" >' + sheep.name + '</p>');
                    });
                });
                $('.day').text('День :' + response.day);
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{route("degree_of_manic")}}',
            success: function (response) {
                $('.degree_of_manic').text('Степень вашей маниакальности = '+response+' жертв');
            }
        });
    };

    var insetMessage = function (message) {
        $('.message_title').remove();
        $('.sheep_name').remove();
        getData();
        $('.message').append(message);
        setTimeout(function () {
            $('.message_title').remove()
        }, 4000);
    };

    var killSheep = function () {
        $.ajax({
            type: 'POST',
            url: '{{route("ajax_kill_sheep")}}',
            success: function (response) {
                if(!response.error) {
                    if (parseInt(response.code) === 0) {
                        $('.message_title').remove();
                        $('.message').append('<h4 class="message_title" data-error="error" style="color:#ff0000">Хватит убивать в каждом загоне осталось по 1 овцы дождитесь рождения новой</h4>');
                        setTimeout(function () {
                            $('.message_title').remove()
                        }, 10000);
                    }else if (parseInt(response.code) === 1) {
                        let message =$('<h4 class="message_title" style="color:blue">' +
                            'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' </h4>');
                        insetMessage(message);
                    } else if (parseInt(response.code) === 2) {
                        let message =$('<h5 class="message_title" style="color:deeppink">' +
                            'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + '' +
                            'и в этот загон была подселена овца :' + response.name_sheep_move +' из '+response.name_from_sheep_pen +' </h5>');
                        insetMessage(message);
                    }else if (parseInt(response.code) === 3) {
                        let  message = $('<h5 class="message_title" style="color:pink">' +
                            'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' Пермещение не удалось осуществить из-за сбоев сервера </h5>');
                        insetMessage(message);
                    }
                }else{
                    $('.message').append('<h4 class="message_title" data-error="error" style="color:#ff0000">Ошибка сервера</h4>');
                    setTimeout(function () {
                        $('.message_title').remove()
                    }, 2000);
                }
            }
        });
    };

    getData();
});