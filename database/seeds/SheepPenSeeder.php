<?php

use App\SheepPen;
use Illuminate\Database\Seeder;

class SheepPenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SheepPen::create(['name' => "Загон №1 для овец"]);
        SheepPen::create(['name' => "Загон №2 для овец"]);
        SheepPen::create(['name' => "Загон №3 для овец"]);
        SheepPen::create(['name' => "Загон №4 для овец"]);
    }
}
