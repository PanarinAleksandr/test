<?php

use App\Sheep;
use Illuminate\Database\Seeder;

class SheepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        while (true){
            $array_sum = [ rand(1,10),  rand(1,10) ,  rand(1,10),  rand(1,10)];
            if(array_sum($array_sum) == 10){
                break;
            }
        }
        $inc = 0;
        foreach($array_sum as $key => $value){
            $id_sheep_pen = $key + 1;
            for($i = 1; $i <= $value ; $i++){
                $inc ++ ;
                $sheep = new Sheep();
                $sheep->name = 'sheep'.$inc;
                $sheep->live = 1;
                $sheep->birthday = 1;
                $sheep->sheep_pen_id = $id_sheep_pen;
                $sheep->day_death = 0;
                $sheep->save();
            }
        }

    }
}
