<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


use Illuminate\Support\Facades\Artisan;

Route::get('/', 'MainController@index')->name('home');
Route::get('/report', 'MainController@report')->name('report');
Route::post('/ajax_report_between_day', 'MainController@ajaxReportBetweenDay')->name('ajax_report_between_day');
Route::post('/retun_all_days', 'MainController@retunAllDays')->name('retun_all_days');
Route::post('/ajax_index', 'MainController@ajaxIndex')->name('ajax_index');
Route::post('/ajax_report', 'MainController@ajaxReport')->name('ajax_report');
Route::post('/ajax_create_sheep', 'MainController@ajaxCreateSheep')->name('ajax_create_sheep');
Route::post('/ajax_kill_sheep', 'MainController@ajaxKillSheep')->name('ajax_kill_sheep');
Route::post('/degree_of_manic', 'MainController@degreeOfManic')->name('degree_of_manic');

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    return "Кэш очищен.";
});

