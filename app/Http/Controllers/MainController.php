<?php

namespace App\Http\Controllers;

use App\Sheep;
use App\SheepPen;
use http\Env\Request;

class MainController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function report()
    {
        return view('report');
    }

    public function ajaxReport()
    {
        $array = $this->reportDays();
        return response($array);
    }

    public function ajaxReportBetweenDay(\Illuminate\Http\Request $request)
    {
        $array = $this->reportDays($request);
        return response($array);
    }

    public function retunAllDays()
    {
        try {
            $array = [];
            $array['days'] = Sheep::select('birthday')->distinct()->get()->count();
        } catch (\Exception $e) {
            $array['error'] = $e->getMessage();
        }
        return response($array);

    }

    public function ajaxIndex()
    {
        $sheep_pens = SheepPen::all();
        $day = Sheep::max('birthday');
        $response = [];
        $arr = [];
        foreach ($sheep_pens as $key => $sheep_pen) {
            $data = ['sheep_pen' => [], 'sheep' => []];
            $data['sheep_pen'] = $sheep_pen;
            $array_sheep = $sheep_pen->sheep->where('live', '=', 1);
            $array = $this->sortKey($array_sheep);
            $data['sheep'] = $array;
            $arr[] = $data;
        }
        $response['data'] = $arr;
        $response['day'] = $day;


        return response($response);
    }

    public function ajaxCreateSheep()
    {
        try {
            $sheep_pens = SheepPen::all();
            $array_sheep = $this->counterLiveOrDieSheep($sheep_pens);
            $array = [];
            while (true) {
                $rand = rand(1, 4);
                foreach ($sheep_pens as $key => $sheep_pen) {

                    $count_sheep = $sheep_pen->sheep->where('sheep_pen_id', '=', $sheep_pen->id)->where('live', '!=', 0)->count();

                    if (($sheep_pen->id == $rand && $count_sheep > 1)
                        || ($sheep_pen->id == $rand && $array_sheep[0] == 1 && $array_sheep[1] == 1 && $array_sheep[2] == 1 && $array_sheep[3] == 1)
                    ) {
                        $sheep_pen_id = $sheep_pen->id;
                        $total = Sheep::all()->count();
                        $day = Sheep::max('birthday');
                        $day += 1;
                        $count = $total + 1;
                        $sheep = new Sheep();
                        $sheep->name = 'Новая овца-' . $count;
                        $sheep->live = 1;
                        $sheep->birthday = $day;
                        $sheep->day_death = 0;
                        $sheep->sheep_pen_id = $sheep_pen_id;
                        $sheep->save();
                        $array['name_sheep'] = $sheep->name;
                        $array['name_sheep_pen'] = SheepPen::find($sheep_pen_id)->name;
                        $array['code'] = 0;
                        break 2;
                    }
                }
            }
            $sheep_pen_all = SheepPen::all();
            $array_sheep = $this->counterLiveOrDieSheep($sheep_pen_all);
            $key_sheep_pen = array_search(1, $array_sheep);

            if ($key_sheep_pen === 0 || $key_sheep_pen) {
                $data = $this->movingSheep($sheep_pen_all[$key_sheep_pen]->id);
                $array['name_sheep_move'] = $data['name_sheep_move'];
                $array['name_from_sheep_pen'] = $data['name_from_sheep_pen'];
                $array['code'] = 1;
            }

        } catch (\Exception $e) {
            $error = $e->getMessage();
            $array['error'] = $error;
        }
        return response($array);
    }

    public function ajaxKillSheep()
    {
        try {

            $sheep_pen_all = SheepPen::all();
            $array_sheep_count = $this->counterLiveOrDieSheep($sheep_pen_all);
            $response = [];

            if ($array_sheep_count[0] == 1 && $array_sheep_count[1] == 1 && $array_sheep_count[2] == 1 && $array_sheep_count[3] == 1) {
                $response['name_sheep'] = null;
                $response['name_sheep_pen'] = null;
                $response['code'] = 0;

            } else {
                while (true) {
                    $rand = rand(1, 4);
                    $sheep_pen = SheepPen::where('id', $rand)->get();

                    $count = $sheep_pen[0]->sheep->where('live', 1)->count();
                    if ($count > 1) {

                        $array_sheep = $this->sortKey($sheep_pen[0]->sheep->where('live', 1));

                        $sheep_rand = array_rand($array_sheep, 1);

                        $day = Sheep::max('birthday');

                        Sheep::where('id', $array_sheep[$sheep_rand]->id)->update(['live' => 0, 'day_death' => $day]);

                        $sheep_dieth = Sheep::find($array_sheep[$sheep_rand]->id);

                        $sheep_pen_die_sheep = SheepPen::find($sheep_dieth->sheep_pen_id);


                        $response['name_sheep'] = $sheep_dieth->name;
                        $response['name_sheep_pen'] = $sheep_pen_die_sheep->name;
                        $response['code'] = 1;

                        $count_live_shep = $sheep_pen_die_sheep->sheep->where('live', 1)->count();

                        $sheep_pen_all2 = SheepPen::all();
                        $array_sheep_count2 = $this->counterLiveOrDieSheep($sheep_pen_all2);

                        if ($count_live_shep === 1 && !($array_sheep_count2[0] == 1 && $array_sheep_count2[1] == 1 && $array_sheep_count2[2] == 1 && $array_sheep_count2[3]) == 1) {

                            $move_sheep = $this->movingSheep($sheep_pen[0]->id);

                            if (array_key_exists('error', $move_sheep)) {
                                $response['code'] = 3;
                                break 1;
                            }
                            $response['name_sheep_move'] = $move_sheep['name_sheep_move'];
                            $response['name_from_sheep_pen'] = $move_sheep['name_from_sheep_pen'];
                            $response['code'] = 2;

                            break 1;
                        }
                        break 1;
                    }
                }
            }
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }
        return response($response);

    }

    public function degreeOfManic()
    {
        $sheep_die = Sheep::where('day_death', '!=', 0)->count();
        return response($sheep_die);
    }

    public function sortKey($array)
    {
        $arr = [];
        foreach ($array as $key => $val) {
            $arr[] = $val;
        }
        return $arr;
    }

    public function counterLiveOrDieSheep($array, $num = 1)
    {
        $arr = [];
        foreach ($array as $key => $val) {
            $count = $val->sheep->where('live', '=', $num)->count();
            $arr[] = $count;
        }
        return $arr;
    }

    public function movingSheep($id_sheep_pen)
    {
        try {
            $array = [];

            $sheep_pen_new = SheepPen::all();
            $array_sheep = $this->counterLiveOrDieSheep($sheep_pen_new);

            $key_sheep_pen_max = array_search(max($array_sheep), $array_sheep);
            $array_clear_key_sheep = $this->sortKey($sheep_pen_new[$key_sheep_pen_max]->sheep->where('live', '=', 1));

            $key_sheep_object = array_rand($array_clear_key_sheep, 1);


            $array['name_sheep_move'] = Sheep::find($array_clear_key_sheep[$key_sheep_object]->id)->name;
            $array['name_from_sheep_pen'] = SheepPen::find($sheep_pen_new[$key_sheep_pen_max])[0]->name;

            Sheep::where('id', $array_clear_key_sheep[$key_sheep_object]->id)->update(['sheep_pen_id' => $id_sheep_pen]);

        } catch (\Exception $e) {

            $array['error'] = $e->getMessage();
        }
        return $array;
    }

    public function reportDays($data = false)
    {
        try {
            if ($data) {
                $day1 = $data['first_day'];
                $day2 = $data['second_day'];
            } else {
                $day1 = 1;
                $day2 = Sheep::select('birthday')->distinct()->get()->count();
            }
            $sheep_pen = SheepPen::all();
            $array_data = [];

            for ($i = $day1; $i <= $day2; $i++) {
                $array = [];
                $total_number_of_sheep = Sheep::all()->where('birthday', '<=', $i)->count();
                $number_of_sheep_killed = Sheep::where('day_death', '!=', 0)->where('day_death', '<=', $i)->where('birthday', '<=', $i)->count();

                $number_of_live_sheep = Sheep::where('birthday', '<=', $i)->where('day_death', '=', 0)->orwhere('birthday', '<=', $i)->where('day_death', '>', $i)->count();

                $arr = [];
                $arr2 = [];
                foreach ($sheep_pen as $val) {
                    $arr[] = $val->sheep->where('live', '!=', 0)->where('birthday', '<=', $i)->count();
                    $arr2[] = $val->sheep->where('day_death', '=', 0)->where('birthday', '<=', $i)->count();
                }
                $num_max = max($arr);
                $id_max = array_keys($arr, $num_max);
                $populated_pen = $sheep_pen[$id_max[0]];

                $num_min = min($arr2);
                $id_min = array_keys($arr2, $num_min);
                $less_populated_pen = $sheep_pen[$id_min[0]];
                $array['total_number_of_sheep'] = $total_number_of_sheep;
                $array['number_of_sheep_killed'] = $number_of_sheep_killed;
                $array['number_of_live_sheep'] = $number_of_live_sheep;
                $array['populated_pen'] = $populated_pen;
                $array['less_populated_pen'] = $less_populated_pen;
                $array['day'] = $i;
                $array_data[] = $array;
            }

        } catch (\Exception $e) {
            $array_data['error'] = $e->getMessage();

        }
        return $array_data;
    }
}
