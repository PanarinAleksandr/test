<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SheepPen extends Model
{

    public function sheep()
    {
        return $this->hasMany('App\Sheep');
    }
}
