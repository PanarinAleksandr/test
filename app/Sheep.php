<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sheep extends Model
{

    public function sheepPen(){
        return $this->belongsTo('App\SheepPen');
    }
}
