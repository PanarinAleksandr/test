<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/index.css') }}" />
    <script src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>

    {{--<script src="{{ asset('js/index.js') }}"></script>--}}

</head>
<body>
<div class="container">

    <h1 style="text-align: center">Ферма</h1>
    <div class="sheep_pen_cont">

        <p id="title_sheep_pen-1" class="title_sheep_pen"></p>

        <div id="sheep_pen_1" class="sheep_pen">
        </div>
    </div>
    <div class="sheep_pen_cont">

        <p id="title_sheep_pen-2" class="title_sheep_pen"></p>

        <div id="sheep_pen_2" class="sheep_pen"></div>
    </div>
    <div class="sheep_pen_cont">

        <p id="title_sheep_pen-3" class="title_sheep_pen"></p>

        <div id="sheep_pen_3" class="sheep_pen"></div>
    </div>
    <div class="sheep_pen_cont">

        <p id="title_sheep_pen-4" class="title_sheep_pen"></p>

        <div id="sheep_pen_4" class="sheep_pen"></div>

    </div>
    <p class="degree_of_manic"> </p>
    <p class="day"></p>
    <div class="message"></div>
    <div>
        <input type="submit" id="update" name="update" value="Обновить" class="btn btn-info">
        <input type="submit" id="kill_sheep" name="kill_sheep" value="Убить овечку" class="btn btn-warning"
               style="float: right">
    </div>
    <div style="margin-top: 10px">
        <input type="text" placeholder="Введите текст команды" size="100">
        <input type="submit" name="send" value="Выполнить">
        <a href="{{route('report')}}" > Отчет </a>
    </div>
</div>

<script >
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update').on('click', function () {
            location.reload();
        });

        let i = 0;

        $('#kill_sheep').on('click', function () {
            killSheep();
        });
        setInterval(function () {
            $.ajax({
                type: 'POST',
                url: '{{route("ajax_create_sheep")}}',
                success: function (response) {
                    if (response.error ) {
                        let message = $('<h4 class="message_title" style="color:red">Ошибка сервера</h4>');
                        insetMessage(message);
                    } else if(response.code === 0){
                        let message = $('<h4 class="message_title" style="color: #2a9055;" > В' + response.name_sheep_pen + ": родилась овца с именем " + response.name_sheep + '</h4>');
                        insetMessage(message);
                    }else if(response.code === 1){
                        let message = $('<h4 class="message_title" style="color:#2a9055"> В' +
                            ''+ response.name_sheep_pen + ": родилась овца с именем " + response.name_sheep + '' +
                            ' После овца по имени '+ response.name_sheep_move +' была пермещена в '+response.name_from_sheep_pen +'</h4>');
                        insetMessage(message);
                    }

                }
            });
        }, 10000);

        setInterval(function () {
            $.ajax({
                type: 'POST',
                url: '{{route("ajax_kill_sheep")}}',
                success: function (response) {
                    console.log(response);
                    if(!response.error) {
                        if (parseInt(response.code) === 0) {
                            console.log(response.code);
                            $('.message_title').remove();
                            $('.message').append('<h4 class="message_title" style="color:red">Хватит убивать в каждом загоне осталось по 1 овцы дождитесь рождения новой</h4>');
                            setTimeout(function () {
                                $('.message_title').remove()
                            }, 10000);
                        }else if (parseInt(response.code) === 1) {
                            let message =$('<h4 class="message_title" style="color:blue">' +
                                'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' </h4>');
                            insetMessage(message);
                        } else if (parseInt(response.code) === 2) {
                            let message =$('<h5 class="message_title" style="color:deeppink">' +
                                'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + '' +
                                'и в этот загон была подселена овца :' + response.name_sheep_move +' из '+response.name_from_sheep_pen +' </h5>');
                            insetMessage(message);
                        }else if (parseInt(response.code) === 3) {
                            let  message = $('<h5 class="message_title" style="color:pink">' +
                                'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' Пермещение не удалось осуществить из-за сбоев сервера </h5>');
                            insetMessage(message);
                        }
                    }else{
                        $('.message').append('<h4 class="message_title" style="color:red">Ошибка сервера</h4>');
                        setTimeout(function () {
                            $('.message_title').remove()
                        }, 2000);
                    }
                }
            });
        }, 100000);

        var getData = function () {
            $.ajax({
                type: 'POST',
                url: '{{route("ajax_index")}}',
                success: function (response) {

                    $.each(response.data, function (key, value) {
                        $('#title_sheep_pen-' + value.sheep_pen.id).text(value.sheep_pen.name);
                        var sheep_pen = $('#sheep_pen_' + value.sheep_pen.id);

                        $.each(value.sheep, function (key1, sheep) {
                            sheep_pen.append('<p class="sheep_name" id="sheep-' + sheep.id + '" >' + sheep.name + '</p>');
                        });
                    });
                    $('.day').text('День :' + response.day);
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{route("degree_of_manic")}}',
                success: function (response) {
                    $('.degree_of_manic').text('Степень вашей маниакальности = '+response+' жертв');
                }
            });
        };

        var insetMessage = function (message) {
            $('.message_title').remove();
            $('.sheep_name').remove();
            getData();
            $('.message').append(message);
            setTimeout(function () {
                $('.message_title').remove()
            }, 4000);
        };

        var killSheep = function () {
            $.ajax({
                type: 'POST',
                url: '{{route("ajax_kill_sheep")}}',
                success: function (response) {
                    if(!response.error) {
                        if (parseInt(response.code) === 0) {
                            $('.message_title').remove();
                            $('.message').append('<h4 class="message_title" data-error="error" style="color:#ff0000">Хватит убивать в каждом загоне осталось по 1 овцы дождитесь рождения новой</h4>');
                            setTimeout(function () {
                                $('.message_title').remove()
                            }, 10000);
                        }else if (parseInt(response.code) === 1) {
                            let message =$('<h4 class="message_title" style="color:blue">' +
                                'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' </h4>');
                            insetMessage(message);
                        } else if (parseInt(response.code) === 2) {
                            let message =$('<h5 class="message_title" style="color:deeppink">' +
                                'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + '' +
                                'и в этот загон была подселена овца :' + response.name_sheep_move +' из '+response.name_from_sheep_pen +' </h5>');
                            insetMessage(message);
                        }else if (parseInt(response.code) === 3) {
                            let  message = $('<h5 class="message_title" style="color:pink">' +
                                'Овца была зарублена по имени :' + response.name_sheep + ' из ' + response.name_sheep_pen + ' Пермещение не удалось осуществить из-за сбоев сервера </h5>');
                            insetMessage(message);
                        }
                    }else{
                        $('.message').append('<h4 class="message_title" data-error="error" style="color:#ff0000">Ошибка сервера</h4>');
                        setTimeout(function () {
                            $('.message_title').remove()
                        }, 2000);
                    }
                }
            });
        };

        getData();
    });
</script>

</body>
</html>



