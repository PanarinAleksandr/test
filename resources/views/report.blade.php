<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/index.css') }}"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>

</head>
<body>

<div class="container error">
    <a href="{{ route('home')  }}">Вернуться на ферму</a>
    <hr>
    <div class="form" style="margin-top: 10px; margin-bottom: 10px">
        <p>Отчет за определенный переод выберете дни </p>
        <select id="first_day" class="change_day form-control" style="width: 200px ; float: left">
            <option value="1" disabled selected>Выбирите день</option>
        </select>
        <select id="second_day" class="change_day form-control" style="width: 200px ; float: left">
            <option value="1" disabled selected>Выбирите день</option>
        </select>
        <input type="submit" class="btn btn-dark">
    </div>

    <table style="width:100%;border: 1px solid grey;" class="data">
        <tr class="table-active">
            <th style="width: 20%">Название самого населённого загона</th>
            <th style="width: 20%">Название самого менее населённого загона</th>
            <th style="width: 18%">Общее количество овечек</th>
            <th style="width: 18%">Общее количество убитых овечек</th>
            <th style="width: 18%">Общее количество живых овечек</th>
            <th style="width: 18%">Дни</th>
        </tr>

    </table>
</div>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{route("ajax_report")}}',
            success: function (response) {
                pastData(response);

            }
        });
        $.post('{{route("retun_all_days")}}', function (response) {
            for (i = 1; i <= response.days; i++) {
                var options = $('<option class="option_day">' + i + '</option>');
                $('.change_day').append(options);
            }
        });

        $('.btn').on('click', function (e) {
            e.preventDefault();
            var first_day = $('#first_day');
            var second_day = $('#second_day');
console.log(first_day.val());
console.log(first_day.val() > second_day.val());
            if (first_day.val() !== null && second_day.val() !== null) {
                var data = {};
                if (first_day.val() <= second_day.val()) {
                    data = {
                        first_day: first_day.val(),
                        second_day: second_day.val()
                    }
                } else if (first_day.val() > second_day.val()) {
                    data = {
                        first_day: second_day.val(),
                        second_day: first_day.val()
                    }
                }
                $.ajax({
                    type: 'POST',
                    url: '{{route("ajax_report_between_day")}}',
                    data: data,
                    success: function (response) {
                        console.log(response)
                        $('.data_tr').remove();
                        pastData(response);
                    }
                });
                first_day.prop('selectedIndex', 0);
                second_day.prop('selectedIndex', 0);
            } else {
                $('.error_message').remove();
                $('.error').prepend('<h4 class="error_message" style="text-align: center; color:yellow">Вы не выбрали дни</h4>')
                setTimeout(function () {
                    $('.error_message').remove()
                }, 2000);

            }
        });

        var pastData = function (data) {
            if (!data.error) {

                var style = {
                    6: "table-secondary",
                    1: "table-success",
                    2: "table-danger",
                    0: "table-warning",
                    4: "table-info",
                    5: "table-light",
                    3: "table-dark"
                };
                $.each(data, function (key, value) {
                    var data = $(
                        '<tr class="' + style[key] + ' data_tr">' +
                        '<td>' + value.populated_pen.name + '</td>' +
                        '<td>' + value.less_populated_pen.name + '</td>' +
                        '<td>' + value.total_number_of_sheep + '</td>' +
                        '<td>' + value.number_of_sheep_killed + '</td>' +
                        '<td>' + value.number_of_live_sheep + '</td>' +
                        '<td> ' + value.day + '</td>' +
                        '</tr>');
                    $('.data').append(data);
                });


            } else {
                $('.form').remove();
                $('.data').remove();
                var error = $('<h1 class="error_message" style="color: red; text-align: center"> Ошибка сервера </h1>');
                $('.error').prepend(error)
                setTimeout(function () {
                    $('.error_message').remove()
                }, 2000);
            }
        }

    });
</script>
</body>
</html>